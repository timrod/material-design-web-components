// Require
const {src, dest, watch, parallel, series} = require('gulp');

//const package = require('./package.json');
//const merge_stream = require('merge-stream');

const del = require('del');
const browser_sync = require('browser-sync').create();
const rename = require('gulp-rename');
const fileinclude = require('gulp-file-include');
const inlineSource = require('gulp-inline-source-html')
const gulp_if = require('gulp-if');
const replace = require('gulp-replace');
const webpack = require('webpack-stream');

const scss = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const group_css_media = require('gulp-group-css-media-queries');
const sourcemaps = require('gulp-sourcemaps');
const clean_css = require('gulp-clean-css');

// Mode
const isProduction = process.env.NODE_ENV === 'production';

// Path
const DIR_SOURCE = 'packages';
const DIR_BUILD = 'build';
const DIR_DIST = 'dist';
const DIR_EXAMPLE = `example`;
const DIR_DESTINATION = isProduction ? DIR_DIST : DIR_BUILD;

let path = {
	source: {
		css : `${DIR_SOURCE}/timrod-mdwc.scss`,
		js  : `${DIR_SOURCE}/timrod-mdwc.js`,
		html: `${DIR_EXAMPLE}/index.html`
	},
	watch : {
		css : `${DIR_SOURCE}/**/*.scss`,
		js  : `${DIR_SOURCE}/**/*.js`,
		html: `${DIR_EXAMPLE}/**/*.*`
	}
}

// Tasks

let task = {};

// Service

task.servise = {
	cleanBuild: function () {
		return del(`./${DIR_BUILD}/*`);
	},
	cleanDist : function () {
		return del(`./${DIR_DIST}/*`);
	},
	watching  : function () {
		watch([path.watch.css], task.build.css);
		watch([path.watch.js], task.build.js);
		watch([path.watch.html], task.build.html);
	},
	browser   : function () {
		browser_sync.init({
			/*server: {
			 baseDir: './' + DIR_BUILD,
			 index  : DIR_BUILD + '/typography.html'
			 }*/
			server: [DIR_BUILD],
			open  : false
		});
	}
}

// Build

task.build = {
	css: function () {
		return src(path.source.css)
			.pipe(
				sourcemaps.init()
			)
			.pipe(
				scss({
					outputStyle: 'expanded'
				}).on('error', scss.logError)
			)
			.pipe(
				autoprefixer({
					cascade: false
				})
			)
			.pipe(
				group_css_media()
			)
			.pipe(
				gulp_if(
					isProduction,
					clean_css()
				)
			)
			.pipe(
				gulp_if(
					isProduction,
					rename({suffix: '.min'})
				)
			)
			.pipe(sourcemaps.write('./'))
			.pipe(
				dest(DIR_DESTINATION)
			)
			.pipe(
				browser_sync.stream()
			);
	},

	js: function () {
		return src(path.source.js)
			.pipe(
				webpack({
					config: require('./webpack.config.js')
				})
			)
			.pipe(
				dest(DIR_DESTINATION)
			)
			.pipe(
				browser_sync.stream()
			);
	},

	html: function () {
		return src(path.source.html)
			.pipe(
				fileinclude()
			)
			.pipe(
				gulp_if(
					isProduction,
					replace('timrod-mdwc', 'timrod-mdwc.min')
				)
			)
			.pipe(
				inlineSource()
			)
			.pipe(
				dest(DIR_DESTINATION)
			)
			.pipe(
				browser_sync.stream()
			);
	}
}


exports.build = series(
	task.servise.cleanBuild,
	parallel(
		task.build.css,
		task.build.js,
		task.build.html
	)
);

exports.default = parallel(
	exports.build,
	task.servise.watching,
	task.servise.browser
);