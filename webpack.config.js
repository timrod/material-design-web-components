const path = require('path');
const isProduction = process.env.NODE_ENV === 'production';
const mode = isProduction ? 'production' : 'development';
const prefix = isProduction ? '.min' : '';

module.exports = {
	mode   : mode,
	entry  : {
		app: './packages/timrod-mdwc.js',
	},
	output : {
		filename: `timrod-mdwc${prefix}.js`,
	},
	devtool: 'source-map',
	module : {
		rules: [
			{
				test   : /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader : 'babel-loader',
				options: {
					presets: ['@babel/preset-env', 'minify'],
				},
			}
		],
	},
};