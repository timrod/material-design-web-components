document.addEventListener('DOMContentLoaded', function () {

	// Demo component switch
	function switchComponents() {
		const select = document.querySelector('.demo-select-component select')

		function hideAllSections() {
			document.querySelectorAll('section').forEach((section) => {
				section.style.display = 'none';
			});
		}

		function getCookie() {
			let cookieSelectComponent = document.cookie.split(';');

			let value;

			cookieSelectComponent.forEach((val) => {
				if (val.includes('selectComponent=')) {
					value = val.split('=')[1]
				}
			});

			return value;
		}

		document.querySelector('.demo-select-component select').addEventListener('change', (event) => {
			update();
		});

		function update() {
			let option = select.options[select.selectedIndex].value;

			hideAllSections();
			let section = document.querySelector(`.demo-section-${option}`);
			section.style.display = 'block';
			document.cookie = `selectComponent=${option}`;
		}

		let cookie = getCookie();

		if (cookie) {
			select.querySelectorAll('option').forEach((el) => {
				el.toggleAttribute('selected', el.value === cookie)
			})
		}

		update();
	}

	switchComponents();

	//Demo checkbox
	document.querySelectorAll('.tmd-checkbox').forEach((label) => {
		if (0 <= label.innerText.indexOf('Indeterminate')) {
			label.querySelector('input').indeterminate = true;
		}
	});

	// List
	// Demo list activated
	const list_activated = document.querySelectorAll('.demo-list-selected > .tmd-list-item');

	for (const element of list_activated) {
		element.addEventListener('click', function (event) {

			let parent = event.target.parentElement;
			let list = parent.querySelectorAll('.tmd-list-item');

			list.forEach(function (e) {
				e.classList.remove('tmd-list-item--selected');
			});

			event.target.classList.add('tmd-list-item--selected');
		});
	}

	// Base
	let base = new tmd.TMDBase({'verbose': 1});
	base.attach('buttonRipple', new tmd.TMDRipple('.tmd-button'));
	base.attach('listRipple', new tmd.TMDRipple('.demo-list-ripple > .tmd-list-item'))
	base.attach('chipRipple', new tmd.TMDRipple('.tmd-chip'))
	base.start();

	base.get('table').getNodeById('table-progress').showProgress();

	// Dialog
	document.addEventListener('TMDDialog:close', function (e) {
		document.getElementById('demo-dialog-message').innerText = 'Declined... Maybe next time?';
	})

	document.addEventListener('TMDDialog:action', function (e) {
		if (e.detail.action === 'accept') {
			e.detail.node.close(e);
			document.getElementById('demo-dialog-message').innerText = 'Accepted, thanks!';
		}
	})

	// Snackbar
	let snackbarButtons = document.querySelectorAll('[data-tmd-snackbar]');

	snackbarButtons.forEach((button) => {
		button.addEventListener('click', (e) => {
			let id = button.getAttribute('data-tmd-snackbar');
			base.get('snackbar').open(id, 5000);
		})
	})
});