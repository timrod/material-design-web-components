import TMDNode from '../../base/js/_node';

export class TMDSliderAdapter extends TMDNode {

	configure() {
		this.name = 'slider';
		this.requireRootClass = true;
	}

	initialize() {
		this.input = this.root.querySelector('.tmd-slider__input');

		this.prepare();

		this.input.addEventListener('input', this.calcProgress.bind(this));

		this.calcProgress();
	}

	prepare() {
		let progress = document.createElement('div');
		progress.classList.add('tmd-slider__progress');
		this.sliderProgress = this.root.appendChild(progress);

		if (this.root.classList.contains('tmd-slider--discrete')) {
			let pin = document.createElement('span');
			pin.classList.add('tmd-slider__pin');
			pin.innerHTML = '<span class="tmd-slider__pin-value-marker"></span>';
			this.sliderPin = this.root.appendChild(pin);
			this.sliderPinValueMarker = this.sliderPin.querySelector('.tmd-slider__pin-value-marker')
		}

		if (this.root.classList.contains('tmd-slider--display-markers')) {
			let step = this.input.step || 1;
			let max = this.input.max || 100;
			let steps = Math.round(max / step);
			this.input.style.setProperty('background', `linear-gradient(to right, currentcolor 2px, transparent 0px) 0px center / calc((100% - 2px) / ${steps}) 100% repeat-x`)
		}
	}

	calcProgress() {
		let value, max, step, percent;

		value = this.input.value;
		step = this.input.step || 1;
		max = this.input.max || 100;

		percent = value / max * 100;
		percent = parseFloat(percent.toFixed(countDecimals(step)));

		this.sliderProgress.style.setProperty('width', `calc(${percent + '%'} - 6px)`);

		if (this.sliderPin) {
			this.sliderPin.style.left = percent + '%';
			this.sliderPinValueMarker.innerText = value;
		}

		function countDecimals(number) {
			number = step.toString().split('.');
			return number.length > 1 ? number[1].length : 0;
		}
	}
}

export default TMDSliderAdapter;