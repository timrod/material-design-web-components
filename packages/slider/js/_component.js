import TMDFoundation from '../../base/js/_foundation';
import TMDSliderAdapter from './_adapter';

export class TMDSlider extends TMDFoundation {

	configure() {
		this.adapter = TMDSliderAdapter;
		this.defaultSelector = '.tmd-slider';
	}
}

export default TMDSlider;