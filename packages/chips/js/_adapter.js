import TMDNode from '../../base/js/_node';

export class TMDChipsAdapter extends TMDNode {
	configure() {
		this.name = 'chip';
		this.chipSet = '';
		this.requireRootClass = true;
	}

	initialize() {
		this.root.tabIndex = -1;

		if (this.root.parentNode.classList.contains('tmd-chip-set')) {
			this.chipSet = this.root.parentNode;

			if (this.chipSet.classList.contains('tmd-chip-set--choice')) {
				this.root.addEventListener('click', this.choice.bind(this));
			}

			if (this.chipSet.classList.contains('tmd-chip-set--filter')) {
				let icon = document.createElement('span');
				icon.classList.add('material-icons', 'tmd-chip__icon', 'tmd-chip__checkmark');
				icon.innerHTML = 'done';

				this.root.prepend(icon);

				this.root.addEventListener('click', this.filter.bind(this));
			}
		}
	}

	choice(e) {
		this.chipSet.querySelectorAll('.tmd-chip').forEach((el) => {
			el.classList.remove('tmd-chip--selected');
		})

		this.root.classList.add('tmd-chip--selected');
		this.dispatch('TMDChips:choice', {node: this});
	}

	filter(e) {
		let toggle = this.root.classList.toggle('tmd-chip--selected');

		let list = [];
		this.chipSet.querySelectorAll('.tmd-chip--selected').forEach((el) => {
			list.push(el);
		})

		this.dispatch('TMDChips:filter', {list: list, node: this});
	}

}

export default TMDChipsAdapter;