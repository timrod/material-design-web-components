import TMDFoundation from '../../base/js/_foundation';
import TMDChipsAdapter from './_adapter';

export class TMDChips extends TMDFoundation {

	configure() {
		this.adapter = TMDChipsAdapter;
		this.defaultSelector = '.tmd-chip';
	}
}

export default TMDChips;