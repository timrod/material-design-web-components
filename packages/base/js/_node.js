/**
 * Node management abstract class
 */
export class TMDNode {

	/**
	 * Main node
	 */
	root;

	/**
	 * Component name
	 */
	name;

	/**
	 *
	 */
	requireRootClass = false;

	/**
	 * Css class names
	 */
	css = {
		base  : '',
		focus : '',
		active: '',
		hover : '',
	}

	/**
	 * Constructor
	 *
	 * @param {Object.<Node>} element Node
	 */
	constructor(element) {
		this.configure();

		if (this.requireRootClass) {
			if (false === element.classList.contains(`tmd-${this.name}`)) {
				return false;
			}
		}

		this.root = element;
		this.css.base = this.css.base ? this.css.base : this.name;
		this.css.focus = `tmd-${this.css.base}--focus`;
		this.css.active = `tmd-${this.css.base}--active`;
		this.css.hover = `tmd-${this.css.base}--hover`;

		if (this.#isUpgraded()) {
			return false;
		} else {
			this.initialize();
			this.#upgrade();
		}
	}

	/**
	 *
	 */
	configure() {
	}

	/**
	 *
	 */
	initialize() {
	}

	/**
	 * Mark node as modified
	 * Necessary to avoid duplication of events
	 */
	#upgrade() {
		let attribute = this.root.getAttribute('data-tmd-upgraded');
		let list = attribute ? attribute.split(',') : [];

		if (false === list.includes(this.name)) {
			list.push(this.name);
		}

		list = list.filter(el => el.length > 0);

		this.root.setAttribute('data-tmd-upgraded', list);
	}

	/**
	 * Checks if a node has been modified
	 */
	#isUpgraded() {
		let attribute = this.root.getAttribute('data-tmd-upgraded');
		let list = attribute ? attribute.split(',') : [];
		return list.includes(this.name);
	}

	/**
	 *
	 * @param name
	 * @param detail
	 */
	dispatch(name, detail = {}) {
		document.dispatchEvent(new CustomEvent(name, {detail: detail}))
	}
}

export default TMDNode;
