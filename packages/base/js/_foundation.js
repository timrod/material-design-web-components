/**
 * An abstract class for storing and managing component nodes.
 */
export class TMDFoundation {

	/**
	 * Adapter
	 */
	adapter;

	/**
	 * Incoming selector
	 */
	selector;

	/**
	 * Default selector
	 * Used when no incoming selector is specified
	 */
	defaultSelector;

	/**
	 *  Node array
	 */
	nodes = [];

	/**
	 * Array of callbacks
	 */
	callbackList = {
		'before': [],
		'after' : []
	}

	/**
	 * Constructor
	 *
	 * @param {String} selector
	 */
	constructor(selector = null) {
		this.selector = selector;
	}

	/**
	 * Initialization
	 */
	initialize() {
		this.configure();
		this.#executeCallback('before');
		this.#attach(this.selector);
		this.#executeCallback('after');
	}

	/**
	 * An abstract method that sets the basic parameters in a child class.
	 * Executed first in `this.initialize()`.
	 */
	configure() {
	}

	/**
	 * Adds callbacks
	 * Type `before` - called before `this.attach()`
	 * Type `after` - called after `this.attach()`
	 *
	 * @param {Function}    callback Callback function
	 * @param {String}      type     Callback type
	 */
	registerCallback(callback, type = 'before') {
		if (typeof callback === 'function') {
			if (this.callbackList.hasOwnProperty(type)) {
				this.callbackList[type].push(callback);
			}
		}
	}

	/**
	 * Sequentially executes all given callback functions from an array `this.callbackList`
	 */
	#executeCallback(type) {
		if (this.callbackList.hasOwnProperty(type)) {
			this.callbackList[type].forEach((callback) => {
				callback();
			});
		}
	}

	/**
	 * Finds elements with the given selector and executes the method `this.create`
	 */
	#attach(selector) {
		if (toString.call(selector) === '[object Null]' || toString.call(selector) === '[object Undefined]') {
			selector = document.querySelectorAll(this.defaultSelector);
		}

		if (toString.call(selector) === '[object String]') {
			selector = document.querySelectorAll(selector);
		}

		if (toString.call(selector) === '[object NodeList]') {
			selector.forEach((element) => {
				this.#create(element)
			});
		}

		if (toString.call(selector).includes('[object HTML')) {
			this.#create(selector)
		}
	}

	/**
	 * Creates an instance of the host adapter
	 */
	#create(element) {
		let object = new this.adapter(element);

		if (object) {
			this.nodes.push(object);
		}
	}

	/**
	 * Gets an array of nodes
	 */
	getNodes() {
		return this.nodes;
	}

	/**
	 * Returns node by id
	 *
	 * @param   {String} id                 ID
	 * @returns {Object.<TMDNode> | null}   TMDNode instance
	 */
	getNodeById(id) {
		let result = null;

		this.nodes.forEach((node) => {
			if (node.root.id === id) {
				result = node;
				return 1;
			}
		});

		return result;
	}

	/**
	 * Returns nodes by class name
	 *
	 * @param   {String} name   Class name
	 * @returns {Array}         TMDNode instances array
	 */
	getNodesByClassName(name) {
		let result = [];

		this.nodes.forEach((node) => {
			if (node.root.classList.contains(name)) {
				result.push(node);
			}
		});

		return result;
	}

	/**
	 * Removes non-existent nodes from the `this.nodes` array
	 */
	cleanUp() {
		let new_list = [];

		this.nodes.forEach((node) => {
			if (node.root.parentElement !== null && node.root.parentNode !== null) {
				new_list.push(node);
			}
		});

		this.nodes = new_list;
	}
}

export default TMDFoundation;
