/**
 * Main class for managing components
 */
export class TMDBase {

	/**
	 * Default Options
	 */
	options = {
		verbose: 0,
	}

	/**
	 * Array of instances
	 */
	objects = new Map();

	/**
	 * List of components for auto-initialization
	 */
	autoInitList = {
		'list'      : 'TMDList',
		'tooltip'   : 'TMDTooltip',
		'text-field': 'TMDTextField',
		'select'    : 'TMDSelect',
		'slider'    : 'TMDSlider',
		'table'     : 'TMDTable',
		'chips'     : 'TMDChips',
		'dialog'    : 'TMDDialog',
		'snackbar'  : 'TMDSnackbar',
		'menu'      : 'TMDMenu',
	}

	/**
	 * Constructor
	 *
	 * @param {Object} options Object with options
	 */
	constructor(options = {}) {
		this.#configure(options);
	}

	/**
	 * Sets allowed options
	 *
	 * @param {Object} options Object with options
	 */
	#configure(options = {}) {
		for (const name in options) {
			if (this.options.hasOwnProperty(name)) {
				this.options[name] = options[name]
			}
		}
	}

	/**
	 * Required method to run logic
	 */
	start() {
		this.#time('base.initialize');

		for (const name in this.autoInitList) {
			let className = this.autoInitList[name]
			if (window.tmd !== 'undefined' && typeof window.tmd[className] !== 'undefined') {
				this.attach(name, new window.tmd[className]());
			}
		}

		this.#initializeAll();

		this.#timeEnd('base.initialize');
	}

	/**
	 * Private initialization method
	 *
	 * @param name
	 */
	#initialize(name) {
		if (typeof this.objects.get(name).initialize === 'function') {
			this.#time(`base.${name}.initialize`)
			this.objects.get(name).initialize();
			this.#timeEnd(`base.${name}.initialize`)
		}
	}

	/**
	 * Calls the `initialize` method on attached objects
	 */
	#initializeAll() {
		this.objects.forEach((o, name) => {
			this.#initialize(name);
		})
	}

	/**
	 * Public initialization method
	 *
	 * @param {String} name Key
	 */
	initialize(name = '') {
		if (name) {
			this.#initialize(name)
		} else {
			this.#initializeAll();
		}
	}

	/**
	 * Attaches an instance of an external object
	 *
	 * @param {String} name                     Key
	 * @param {Object.<TMDFoundation>} object   Object instance
	 */
	attach(name, object) {
		this.objects.set(name, object);
	}

	/**
	 * Returns an object instance by its name
	 *
	 * @param {String} name  Key
	 *
	 * @returns {Object.<TMDFoundation>} Object instance
	 */
	get(name) {
		return this.objects.get(name);
	}

	/**
	 *
	 * @param name
	 */
	#time(name) {
		if (this.options.verbose) {
			console.time(name)
		}
	}

	/**
	 *
	 * @param name
	 */
	#timeEnd(name) {
		if (this.options.verbose) {
			console.timeEnd(name)
		}
	}
}

export default TMDBase;