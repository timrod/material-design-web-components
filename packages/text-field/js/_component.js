import TMDFoundation from '../../base/js/_foundation';
import TMDTextFieldAdapter from './_adapter';

export class TMDTextField extends TMDFoundation {

	configure() {
		this.adapter = TMDTextFieldAdapter;
		this.defaultSelector = '.tmd-text-field';
	}
}

export default TMDTextField;