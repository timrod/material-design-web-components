import TMDNode from '../../base/js/_node';

export class TMDTextFieldAdapter extends TMDNode {
	configure() {
		this.name = 'text-field';
		this.requireRootClass = true;
	}

	initialize() {
		this.input = this.root.querySelector('.tmd-text-field__input');

		this.input.addEventListener('focus', this.handler.bind(this));
		this.input.addEventListener('blur', this.handler.bind(this));

		this.input.addEventListener('input', this.handler.bind(this));

		this.counter();
	}

	handler(e) {
		switch (e.type) {
			case 'focus':
			case 'blur':
				this.root.classList.toggle('tmd-text-field--focus', e.type === 'focus');
				break;
			case 'input':
				this.root.classList.toggle('tmd-text-field--selected', this.input.value.length > 0);
				this.counter();
				break;
		}
	}

	counter() {
		let counter = null;
		let helperLine = this.root.nextElementSibling;

		if (helperLine) {
			counter = helperLine.querySelector('.tmd-text-field__character-counter');
		}

		if (counter) {
			let maxlength = this.input.getAttribute('maxlength');
			let max = maxlength ? ' / ' + maxlength : '';
			let length = this.input.value.length;
			counter.innerHTML = `${length}${max}`;
		}
	}
}

export default TMDTextFieldAdapter;