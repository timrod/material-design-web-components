import TMDFoundation from '../../base/js/_foundation';
import TMDListAdapter from './_adapter';

export class TMDList extends TMDFoundation {

	configure() {
		this.adapter = TMDListAdapter;
		this.defaultSelector = '.tmd-list';
	}
}

export default TMDList;