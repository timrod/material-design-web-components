import TMDNode from '../../base/js/_node';

export class TMDListAdapter extends TMDNode {
	configure() {
		this.name = 'list';
		this.requireRootClass = true;
	}

	initialize() {
		this.root.querySelectorAll('.tmd-list-item').forEach((el) => {
			el.tabIndex = -1;
		})
	}
}

export default TMDListAdapter;