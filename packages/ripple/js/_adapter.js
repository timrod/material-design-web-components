import TMDNode from '../../base/js/_node';

export class TMDRippleAdapter extends TMDNode {
	configure() {
		this.name = 'ripple';
		this.requireRootClass = false;
	}

	initialize() {
		this.root.classList.toggle('tmd-ripple-upgraded', true);
		this.root.addEventListener('click', this.handler.bind(this));
	}

	handler(event) {
		const el = event.currentTarget;

		const circle = document.createElement('div');
		const diameter = Math.max(el.clientWidth, el.clientHeight);
		const radius = diameter / 2;

		circle.style.width = circle.style.height = `${ diameter }px`;
		circle.style.left = `${ event.pageX - el.offsetLeft - radius }px`;
		circle.style.top = `${ event.pageY - el.offsetTop - radius }px`;
		circle.classList.add('tmd-ripple');

		const ripple = el.querySelector('.tmd-ripple');

		if (ripple) {
			ripple.remove();
		}

		el.appendChild(circle);
	}

}

export default TMDRippleAdapter;