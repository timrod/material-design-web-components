import TMDFoundation from '../../base/js/_foundation';
import TMDRippleAdapter from './_adapter';

export class TMDRipple extends TMDFoundation {

	configure() {
		this.adapter = TMDRippleAdapter;
	}
}

export default TMDRipple;