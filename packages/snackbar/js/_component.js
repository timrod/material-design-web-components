import TMDFoundation from '../../base/js/_foundation';
import TMDSnackbarAdapter from './_adapter';

export class TMDSnackbar extends TMDFoundation {

	queue = [];

	configure() {
		this.adapter = TMDSnackbarAdapter;
		this.defaultSelector = '.tmd-snackbar';

		this.registerCallback(this.listener.bind(this), 'before');
	}

	listener() {
		document.addEventListener('TMDSnackbar:close', (e) => {
			this.next();
		})
	}

	open(id, timeout = 5000) {
		let node = this.getNodeById(id);

		if (node) {
			node.setTimeout(timeout);
			this.queue.push(node);

			if (this.queue.length === 1) {
				node.open();
			} else {
				this.next();
			}
		}
	}

	next() {
		if (this.queue.length) {
			if (this.queue[0].isOpened() === false) {
				this.queue.shift();

				if (this.queue.length) {
					this.queue[0].open();
				}
			}
		}
	}
}

export default TMDSnackbar;