import TMDNode from '../../base/js/_node';

export class TMDSnackbarAdapter extends TMDNode {

	timeout = 5000;
	timerAutoClose;

	configure() {
		this.name = 'snackbar';
		this.requireRootClass = true;
	}

	initialize() {
		this.buttons = this.root.querySelectorAll('[data-tmd-snackbar-action]');

		this.buttons.forEach((button) => {
			button.addEventListener('click', this.action.bind(this))
		})
	}

	action(e) {
		let action = e.target.getAttribute('data-tmd-snackbar-action');

		if (action === 'close') {
			this.close();
		}

		this.dispatch('TMDSnackbar:action', {action: action, node: this});
	}

	setTimeout(timeout) {
		if (timeout > 0 && timeout < 4000) {
			this.timeout = 4000
		} else if (timeout > 10000) {
			this.timeout = 10000;
		} else {
			this.timeout = timeout
		}
	}

	open() {
		this.root.classList.add('tmd-snackbar--opening');

		setTimeout(() => {
			this.root.classList.add('tmd-snackbar--open');
		}, 150);

		setTimeout(() => {
			this.root.classList.remove('tmd-snackbar--opening');
			this.dispatch('TMDSnackbar:open', {node: this});

			if (this.timeout > 0) {
				this.timerAutoClose = setTimeout(() => {
					this.close();
				}, this.timeout);
			}

		}, 300);
	}

	close() {
		this.root.classList.add('tmd-snackbar--closing');
		this.root.classList.remove('tmd-snackbar--open');

		clearTimeout(this.timerAutoClose);

		setTimeout(() => {
			this.root.classList.remove('tmd-snackbar--closing');
			this.dispatch('TMDSnackbar:close', {node: this});
		}, 200);
	}

	isOpened() {
		return this.root.classList.contains('tmd-snackbar--open');
	}
}

export default TMDSnackbarAdapter;