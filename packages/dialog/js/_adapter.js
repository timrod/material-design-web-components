import TMDNode from '../../base/js/_node';

export class TMDDialogAdapter extends TMDNode {

	configure() {
		this.name = 'dialog';
		this.requireRootClass = true;
	}

	initialize() {
		this.scrim = this.root.querySelector('.tmd-dialog__scrim');

		if (this.scrim) {
			this.scrim.addEventListener('click', this.close.bind(this));
		}

		this.buttons = this.root.querySelectorAll('[data-tmd-dialog-action]');

		this.buttons.forEach((button) => {
			button.addEventListener('click', this.action.bind(this))
		})
	}

	action(e) {
		let action = e.target.getAttribute('data-tmd-dialog-action');

		if (action === 'close') {
			this.close();
		}

		this.dispatch('TMDDialog:action', {action: action, node: this});
	}

	open() {
		this.root.classList.add('tmd-dialog--opening');

		setTimeout(() => {
			this.root.classList.add('tmd-dialog--open');
		}, 150);

		setTimeout(() => {
			this.root.classList.remove('tmd-dialog--opening');
		}, 300);

		this.dispatch('TMDDialog:open', {node: this});
	}

	close() {
		this.root.classList.add('tmd-dialog--closing');
		this.root.classList.remove('tmd-dialog--open');

		setTimeout(() => {
			this.root.classList.remove('tmd-dialog--closing');
		}, 150);

		this.dispatch('TMDDialog:close', {node: this});
	}
}

export default TMDDialogAdapter;