import TMDFoundation from '../../base/js/_foundation';
import TMDDialogAdapter from './_adapter';

export class TMDDialog extends TMDFoundation {

	configure() {
		this.adapter = TMDDialogAdapter;
		this.defaultSelector = '.tmd-dialog';
		this.registerCallback(this.findButtons.bind(this), 'before');
	}

	findButtons() {
		this.buttons = document.querySelectorAll('[data-tmd-dialog]');

		this.buttons.forEach((button) => {
			button.addEventListener('click', this.buttonTrigger.bind(this))
		})
	}

	buttonTrigger(e) {
		let id = e.target.getAttribute('data-tmd-dialog');
		let node = this.getNodeById(id);
		if (node) {
			node.open();
		}
	}
}

export default TMDDialog;