import TMDFoundation from '../../base/js/_foundation';
import TMDTooltipAdapter from './_adapter';

export class TMDTooltip extends TMDFoundation {

	configure() {
		this.adapter = TMDTooltipAdapter;
		this.defaultSelector = '[data-tmd-tooltip]';
		this.registerCallback(this.createBox, 'before');
	}

	createBox() {
		let box = document.querySelector('.tmd-tooltip-box');

		if (box === null || box === undefined) {
			box = document.createElement('div');
			box.classList.add('tmd-tooltip-box');
			document.querySelector('body').appendChild(box);
		}
	}
}

export default TMDTooltip;