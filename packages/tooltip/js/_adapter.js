import TMDNode from '../../base/js/_node';

export class TMDTooltipAdapter extends TMDNode {
	configure() {
		this.name = 'tooltip';
		this.requireRootClass = false;
	}

	initialize() {
		this.root.addEventListener('mouseenter', this.handler.bind(this));
		this.root.addEventListener('mouseleave', this.handler.bind(this));
	}

	handler(e) {
		if (this.tooltip === undefined) {
			this.create();
		}

		this.tooltip.classList.toggle(this.css.active, e.type === 'mouseenter');
	}

	create() {
		let tooltip = document.createElement('div');
		tooltip.classList.add('tmd-tooltip');
		tooltip.innerText = this.root.getAttribute('data-tmd-tooltip');

		this.tooltip = document.querySelector('.tmd-tooltip-box').appendChild(tooltip);

		let calc = this.calc();

		this.tooltip.style.left = calc.left + 'px';
		this.tooltip.style.top = calc.top + 'px';
		this.tooltip.style.transformOrigin = calc.transform;
	}

	calc() {
		let target = this.root;
		let tooltip = this.tooltip;

		let position = target.getAttribute('data-tmd-tooltip-position') || 'top';
		let targetCenterX = target.offsetLeft + target.offsetWidth / 2;
		let targetCenterY = target.offsetTop + target.offsetHeight / 2;

		let left, top, transform;
		let margin = 5;

		let align = {
			vertical  : {
				top   : target.offsetTop - tooltip.offsetHeight - margin,
				middle: targetCenterY - tooltip.offsetHeight / 2,
				bottom: target.offsetTop + target.offsetHeight + margin
			},
			horizontal: {
				left  : target.offsetLeft - tooltip.offsetWidth - margin,
				center: targetCenterX - tooltip.offsetWidth / 2,
				right : target.offsetLeft + target.offsetWidth + margin
			}
		}

		let transform_types = {
			'top'         : '50% 100%',
			'top-right'   : '0 100%',
			'top-left'    : '100% 100%',
			'right'       : '0 50%',
			'left'        : '100% 50%',
			'bottom-right': '0 0',
			'bottom-left' : ' 100% 0',
			'bottom'      : '50% 0'
		};

		transform = transform_types[position];

		switch (position) {
			case 'top':
				top = align.vertical.top;
				left = align.horizontal.center;
				break;
			case 'top-left':
				top = align.vertical.top;
				left = align.horizontal.left;
				break;
			case 'top-right':
				top = align.vertical.top;
				left = align.horizontal.right;
				break;
			case 'right':
				top = align.vertical.middle;
				left = align.horizontal.right;
				break;
			case 'left':
				top = align.vertical.middle;
				left = align.horizontal.left;
				break;
			case 'bottom':
				top = align.vertical.bottom;
				left = align.horizontal.center;
				break;
			case 'bottom-left':
				top = align.vertical.bottom;
				left = align.horizontal.left;
				break;
			case 'bottom-right':
				top = align.vertical.bottom;
				left = align.horizontal.right;
				break;
		}

		// Beyond screen
		if (left < 20) {
			left = target.offsetLeft;
			transform = 'unset';
		}

		if (left >= document.body.clientWidth) {
			left = target.offsetLeft + target.offsetWidth - tooltip.offsetWidth;
			transform = 'unset';
		}

		if (top < 20) {
			top = target.offsetTop + target.offsetHeight + margin;
			transform = 'unset';
		}

		return {top, left, transform};
	}
}

export default TMDTooltipAdapter;