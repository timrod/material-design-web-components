import { TMDBase } from './base';
import { TMDSlider } from './slider';
import { TMDList } from './list';
import { TMDSelect } from './select';
import { TMDTextField } from './text-field';
import { TMDTooltip } from './tooltip';
import { TMDRipple } from './ripple';
import { TMDTable } from './table';
import { TMDChips } from './chips';
import { TMDDialog } from './dialog';
import { TMDSnackbar } from './snackbar';
import { TMDMenu } from './menu';


window.tmd = {
	TMDBase     : TMDBase,
	TMDSlider   : TMDSlider,
	TMDList     : TMDList,
	TMDSelect   : TMDSelect,
	TMDTextField: TMDTextField,
	TMDTooltip  : TMDTooltip,
	TMDRipple   : TMDRipple,
	TMDTable    : TMDTable,
	TMDChips    : TMDChips,
	TMDDialog   : TMDDialog,
	TMDSnackbar : TMDSnackbar,
	TMDMenu     : TMDMenu,
};
