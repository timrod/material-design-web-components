import TMDNode from '../../base/js/_node';

export class TMDTableAdapter extends TMDNode {
	configure() {
		this.name = 'table';
		this.requireRootClass = true;
		this.progress = null;
	}

	initialize() {
		this.checkboxes = this.root.querySelectorAll('.tmd-table__cell--checkbox .tmd-checkbox__input');
		this.rootCheckbox = this.root.querySelector('.tmd-table__header-cell--checkbox .tmd-checkbox__input');

		if (this.checkboxes.length) {
			this.checkboxes.forEach((checkbox) => {
				checkbox.addEventListener('change', this.handler.bind(this))
			})

			this.updateRootCheckbox();
			this.updateSelected();
		}

		if (this.rootCheckbox) {
			this.rootCheckbox.addEventListener('change', this.handler.bind(this));
		}
	}

	handler(e) {
		switch (true) {
			case Boolean(e.target.closest('.tmd-table__row')):
				this.updateRootCheckbox();
				this.updateSelected();
				break;
			case Boolean(e.target.closest('.tmd-table__header-row')):
				this.checkboxes.forEach((checkbox) => {
					checkbox.checked = this.rootCheckbox.checked;
				})
				this.updateSelected();
				break;
		}

	}

	updateRootCheckbox() {
		let total = this.checkboxes.length;
		let checked = this.root.querySelectorAll('.tmd-table__cell--checkbox .tmd-checkbox__input:checked').length
		this.rootCheckbox.checked = checked === total;
		this.rootCheckbox.indeterminate = checked < total && checked !== 0;
	}

	updateSelected() {
		this.checkboxes.forEach((checkbox) => {
			checkbox.closest('.tmd-table__row').classList.toggle('tmd-table__row--state-selected', checkbox.checked);
		})
	}

	showProgress() {
		this.progress = this.root.querySelector('.tmd-table__progress-indicator');

		if (this.progress === null) {
			let div = document.createElement('div');
			div.classList.add('tmd-table__progress-indicator');
			div.innerHTML = `<progress class="tmd-progress-linear"></progress>`
			this.progress = this.root.appendChild(div);
		}

		this.progress.classList.add('tmd-table__progress-indicator--visible')
	}

	hideProgress() {
		if (this.progress) {
			this.progress.classList.remove('tmd-table__progress-indicator--visible')
		}
	}
}

export default TMDTableAdapter;