import TMDFoundation from '../../base/js/_foundation';
import TMDTableAdapter from './_adapter';

export class TMDTable extends TMDFoundation {

	configure() {
		this.adapter = TMDTableAdapter;
		this.defaultSelector = '.tmd-table';
	}
}

export default TMDTable;