import TMDFoundation from '../../base/js/_foundation';
import TMDSelectAdapter from './_adapter';

export class TMDSelect extends TMDFoundation {

	configure() {
		this.adapter = TMDSelectAdapter;
		this.defaultSelector = '.tmd-select';
	}
}

export default TMDSelect;