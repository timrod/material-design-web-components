import TMDNode from '../../base/js/_node';
import { TMDList } from '../../list';

export class TMDSelectAdapter extends TMDNode {
	configure() {
		this.name = 'select';
		this.requireRootClass = true;
	}

	initialize() {
		this.select = this.root.querySelector('select');
		this.root.tabIndex = -1;

		// Focus
		this.root.addEventListener('focusin', this.handler.bind(this));
		this.root.addEventListener('focusout', this.handler.bind(this));

		// Click
		this.root.addEventListener('click', this.handler.bind(this));

		this.update();
	}

	handler(e) {
		if (this.list === undefined) {
			this.createList();
			this.update();
		}

		switch (e.type) {
			case 'focusin':
			case 'focusout':
				this.root.classList.toggle(this.css.focus, e.type === 'focusin');
				if (this.root.classList.contains(this.css.focus) === false) {
					this.toggleList(false);
				}
				break;
			case 'click':
				if (e.target.classList.contains('tmd-select__anchor')) {
					this.toggleList();
				}

		}
	}

	listHandler(e) {
		this.select.querySelectorAll('option').forEach((el) => {
			el.toggleAttribute('selected', el.value === e.target.getAttribute('data-value'))
		})

		this.update();
		this.toggleList(false);
		this.select.dispatchEvent(new Event('change'));
	}

	toggleList(flag = undefined) {
		let list = this.root.querySelector('.tmd-list');
		let targetRect = this.root.getBoundingClientRect();
		let position = window.innerHeight - targetRect.bottom - this.root.offsetHeight < list.offsetHeight;

		list.classList.toggle('tmd-select__list--top', position);

		list.style.maxHeight = `${window.innerHeight - 64 - this.root.offsetHeight}px`

		setTimeout(() => {
			list.classList.toggle('tmd-select__list--open', flag);
			this.root.classList.toggle('tmd-select--list-opened', flag);
		}, 100)
	}

	createList() {
		let options = this.select.querySelectorAll('option');

		let list = document.createElement('ul');
		list.classList.add('tmd-list');
		list.classList.add('tmd-select__list');

		let html = ''

		options.forEach((option) => {
			html += `<li class="tmd-list-item" data-value="${option.value}">${option.innerText}</li>`
		});

		list.innerHTML = html;

		this.list = this.root.appendChild(list);
		this.listObject = new TMDList(this.list);
		this.listObject.initialize();

		this.list.addEventListener('mousedown', this.listHandler.bind(this))
	}

	update() {
		let value = this.select.value;

		this.root.classList.toggle('tmd-select--selected', value.length > 0);

		let checkedOption = this.select.querySelector('option:checked')
		this.root.querySelector('.tmd-select__anchor').innerText = checkedOption ? checkedOption.innerText : '';

		if (this.list) {
			this.list.querySelectorAll('.tmd-list-item').forEach((item) => {
				item.classList.toggle('tmd-list-item--selected', value === item.getAttribute('data-value'))
			});
		}
	}
}

export default TMDSelectAdapter;