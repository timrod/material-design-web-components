import TMDFoundation from '../../base/js/_foundation';
import TMDMenuAdapter from './_adapter';

export class TMDMenu extends TMDFoundation {

	configure() {
		this.adapter = TMDMenuAdapter;
		this.defaultSelector = '.tmd-menu';
	}
}

export default TMDMenu;