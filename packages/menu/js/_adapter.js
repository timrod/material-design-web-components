import TMDNode from '../../base/js/_node';
import { TMDList } from '../../list';

export class TMDMenuAdapter extends TMDNode {
	configure() {
		this.name = 'menu';
		this.requireRootClass = true;
	}

	initialize() {
		this.list = this.root.querySelector('.tmd-list');
		this.list.tabIndex = -1;

		let previous = this.root.previousElementSibling;

		if (previous.classList.contains('tmd-menu__button')) {
			this.button = previous;
			this.button.tabIndex = -1;
			this.button.addEventListener('click', this.handler.bind(this));
			this.button.addEventListener('focusout', this.handler.bind(this));
		}
	}

	handler(e) {
		switch (e.type) {
			case 'focusout':
				this.toggleList(false);
				break;
			case 'click':
				this.toggleList();
				break;
		}
	}

	position() {
		let button_bound = this.button.getBoundingClientRect();

		let root_bound = this.root.getBoundingClientRect();

		let margin_x = root_bound.x - button_bound.x;
		margin_x = margin_x > 0 ? margin_x : 0;

		if (margin_x) {
			this.list.style.left = `-${margin_x}px`;
		}

		let margin_y = button_bound.y + button_bound.height - root_bound.y;
		margin_y = margin_y > 0 ? margin_y : 0;

		if (margin_y) {
			this.list.style.paddingTop = `${margin_y}px`;
		}

		console.log(button_bound.y)

		console.log(root_bound.y)

		console.log(this.list.getBoundingClientRect().y)
	}

	toggleList(flag = undefined) {
		let targetRect = this.root.getBoundingClientRect();

		let bottom = window.innerHeight - targetRect.top - this.root.offsetHeight;
		let top = targetRect.top;

		let position = top > bottom;
		let height = top > bottom && top > 100 ? top - 32 : bottom - 32;
		height = height > 100 ? height : 100;

		if (position) {
			this.list.classList.toggle('tmd-menu__list--top', position);
			this.list.style.bottom = this.button.offsetHeight + 16 + 'px';
		}

		this.list.style.maxHeight = `${height}px`;

		this.position();

		setTimeout(() => {
			this.list.classList.toggle('tmd-menu__list--open', flag);
			this.root.classList.toggle('tmd-menu--list-opened', flag);
		}, 100)
	}
}

export default TMDMenuAdapter;